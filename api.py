import random

from fastapi import APIRouter


router = APIRouter()


@router.get("/")
async def read(n: int):
    list_color = list(range(1, 101))
    if n in list_color:
        color = random.choices(
            population=["синий", "зеленый", "красный"],
            weights=[0.55, 0.33, 0.12],
            k=1
        )[0]

        return {"Ваше число": n, "Цвет": color}
    else:
        return "Выберете число от 1 до 100"

